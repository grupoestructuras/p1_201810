package view;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

import controller.Controller;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.LinkedList;
import model.data_structures.Nodo;
import model.logic.TaxiTripsManager;
import model.vo.Compania;
import model.vo.CompaniaServicios;
import model.vo.CompaniaTaxi;
import model.vo.FechaServicios;
import model.vo.InfoTaxiRango;
import model.vo.RangoDistancia;
import model.vo.RangoFechaHora;
import model.vo.Servicio;
import model.vo.ServiciosValorPagado;
import model.vo.Taxi;
import model.vo.ZonaServicios;

/**
 * view del programa
 */
public class TaxiTripsManagerView 
{

	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sss");

		boolean fin=false;
		while(!fin)
		{
			//imprime menu
			printMenu();

			//opcion req
			int option = sc.nextInt();

			switch(option)
			{
			//1C cargar informacion dada
			case 1:

				//imprime menu cargar
				printMenuCargar();

				//opcion cargar
				int optionCargar = sc.nextInt();

				//directorio json
				String linkJson = "";
				switch (optionCargar)
				{
				//direccion json pequeno
				case 1:

					linkJson = TaxiTripsManager.DIRECCION_SMALL_JSON;
					break;

					//direccion json mediano
				case 2:

					linkJson = TaxiTripsManager.DIRECCION_MEDIUM_JSON;
					break;

					//direccion json grande
				case 3:

					linkJson = TaxiTripsManager.DIRECCION_LARGE_JSON;
					break;
				}

				//Memoria y tiempo
				long memoryBeforeCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				long startTime = System.nanoTime();

				//Cargar data
				Controller.cargarSistema(linkJson);

				//Tiempo en cargar
				long endTime = System.nanoTime();
				long duration = (endTime - startTime)/(1000000);

				//Memoria usada
				long memoryAfterCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				System.out.println("Tiempo en cargar: " + duration + " milisegundos \nMemoria utilizada:  "+ ((memoryAfterCase1 - memoryBeforeCase1)/1000000.0) + " MB");

				break;

				//1A	
			case 2:

				//fecha inicial
				System.out.println("Ingrese la fecha inicial y la hora (Ej : 2017-02-01T09:00:00.000)");
				String fechaInicialReq1A = sc.next();
				Date fechitaInicial;

				//fecha final
				System.out.println("Ingrese la fecha final y la hora (Ej : 2017-02-01T09:00:00.000)");
				String fechaFinalReq1A = sc.next();
				Date fechitaFinal;

				try {
					fechitaInicial = sdf.parse(fechaInicialReq1A);
					fechitaFinal = sdf.parse(fechaFinalReq1A);

					//VORangoFechaHora
					RangoFechaHora rangoReq1A = new RangoFechaHora(fechaInicialReq1A, fechaFinalReq1A);

					//Se obtiene la queue dada el rango
					IQueue<Servicio> colaReq1A = Controller.darServiciosEnRango(rangoReq1A);
					int tam = colaReq1A.size();
					Nodo<Servicio> xd = colaReq1A.gethead();
					while(tam !=0)
					{
						System.out.println(xd.darItem().getTripId());
						tam--;
						xd = xd.next();
					}
				} catch (Exception e) {
					// TODO: handle exception
					System.out.println("No se parseo la fecha de 1A taxiView");
				}
				break;

			case 3: //2A

				//comany
				System.out.println("Ingrese el nombre de la compania");
				String companyReq2A = sc.next();

				//fecha inicial
				System.out.println("Ingrese la fecha inicial (Ej : 2017-02-01)");
				String fechaInicialReq2A = sc.next();

				//fecha final
				System.out.println("Ingrese la fecha final (Ej : 2017-02-01)");
				String fechaFinalReq2A = sc.next();

				try {
					Date fechita2 = sdf.parse(fechaInicialReq2A);
					Date fechita2b = sdf.parse(fechaFinalReq2A);
					//VORangoFechaHora
					RangoFechaHora rangoReq2A = new RangoFechaHora(fechaInicialReq2A, fechaFinalReq2A);
					Taxi taxiReq2A = Controller.darTaxiConMasServiciosEnCompaniaYRango(rangoReq2A, companyReq2A);

					if(taxiReq2A!=null)
						System.out.println("El taxi com mas servicios es: " + taxiReq2A.getTaxiId());
					else
						System.out.println("No hay taxis con mas servicios iniciados en ese rango.");
				} catch (Exception e) {
					// TODO: handle exception
					System.out.println("No se pudo pasear la fecha de 2A taxiView");
				}

				break;

			case 4: //3A

				//comany
				System.out.println("Ingrese el id del taxi");
				String idTaxiReq3A = sc.next();

				//fecha inicial
				System.out.println("Ingrese la fecha inicial (Ej : 2017-02-01)");
				String fechaInicialReq3A = sc.next();

				//fecha final
				System.out.println("Ingrese la fecha final (Ej : 2017-02-01)");
				String fechaFinalReq3A = sc.next();
				try {
					Date fechita3 = sdf.parse(fechaInicialReq3A);
					Date fechita3b = sdf.parse(fechaFinalReq3A);

					//VORangoFechaHora
					RangoFechaHora rangoReq3A = new RangoFechaHora(fechaInicialReq3A, fechaFinalReq3A);
					InfoTaxiRango taxiReq3A = Controller.darInformacionTaxiEnRango(idTaxiReq3A, rangoReq3A);
					System.out.println(taxiReq3A);

				} catch (Exception e) {
					System.out.println("No se parseo la 3A de taxiView");
				}

				break;

			case 5: //4A

				//fecha 
				System.out.println("Ingrese la fecha inicial (Ej : 2017-02-01)");
				String fechaReq4A = sc.next();

				//hora inicial
				System.out.println("Ingrese la hora inicial (Ej: 09:00:00.000)");
				String horaInicialReq4A = sc.next();

				//hora final
				System.out.println("Ingrese la hora final (Ej: 09:00:00.000)");
				String horaFinalReq4A = sc.next();

				try {

					Date fechita4 = sdf.parse(fechaReq4A+"T"+horaInicialReq4A);
					Date fechita4b = sdf.parse(fechaReq4A+'T'+horaFinalReq4A);
					//VORangoFechaHora
					RangoFechaHora rangoReq4A = new RangoFechaHora( fechaReq4A+horaInicialReq4A, fechaReq4A+horaFinalReq4A );

					LinkedList<RangoDistancia> listaReq4A = Controller.darListaRangosDistancia(fechaReq4A, horaInicialReq4A, horaFinalReq4A);

					Nodo<RangoDistancia> xd = listaReq4A.primero();
					int l = listaReq4A.size();

					while(l !=0)
					{
						Nodo <Servicio> ser = xd.darItem().getServiciosEnRango().primero();
						int k = xd.darItem().getServiciosEnRango().size();

						while(k!=0)
						{
							if(ser.darItem()!= null)
							{
								System.out.println(ser.darItem().getTripId()+ " limite inferior: " 
										+ xd.darItem().getLimineInferior() + " limite superior: "+ xd.darItem().getLimiteSuperior() );
							}
							else
								System.out.println("No existen taxis en este rango.");

							k--;
							ser = ser.next();
						}
						xd = xd.next();
						l--;
					}

				} catch (Exception e) {
					// TODO: handle exception
				}

				break;

			case 6: //1B
				LinkedList<Compania> lista=Controller.darCompaniasTaxisInscritos();

				System.out.println("El numero de las companias con almenos un taxi inscrito es: "+ lista.size());
				Nodo<Compania> xd = lista.primero();
				int r = lista.size();
				int cont = 0;
				while(r!=0)
				{
					cont+= xd.darItem().getTaxisInscritos().size();
					System.out.println("La compania "+ xd.darItem().getNombre()
							+ " Tiene " + xd.darItem().getTaxisInscritos().size()
							+ "taxis");
					r--;
					xd = xd.next();
				}
				System.out.println("EL total de taxis es de:" + cont);
				break;

			case 7: //2B
				//fecha inicial
				System.out.println("Ingrese la fecha inicial (Ej : yyyy-MM-dd'T'HH:mm:ss.sss)");
				String fechaInicialReq2B = sc.next();

				//fecha final
				System.out.println("Ingrese la fecha final (Ej : yyyy-MM-dd'T'HH:mm:ss.sss)");
				String fechaFinalReq2B = sc.next();

				try {
					Date fechita7 = sdf.parse(fechaInicialReq2B);
					Date fechita7B = sdf.parse(fechaFinalReq2B);

					//Compania
					System.out.println("Ingrese el nobre de la compania");
					String compania2B=sc.next();

					//VORangoFechaHora
					RangoFechaHora rangoReq2B = new RangoFechaHora(fechaInicialReq2B, fechaFinalReq2B);

					Taxi taxi=Controller.darTaxiMayorFacturacion(rangoReq2B, compania2B);

					if(taxi!=null)
					{
						System.out.println("EL taxi con mas facturacion es: "
								+ taxi.getTaxiId()+ " Con un total de:" 
								+ taxi.getCosto());
					}
					else {
						System.out.println("No hay taxis con facturacion en ese rango");
					}

				} catch (Exception e) {
					System.out.println("NO se parseo 2B taxiView");
				}
				break;

			case 8: //3B
				//Compania
				System.out.println("Ingrese el id de la zona");
				String zona3B=sc.next();
				int z = Integer.parseInt(zona3B);


				//fecha inicial
				System.out.println("Ingrese la fecha inicial (Ej : 2017-02-01)");
				String fechaInicialReq3B = sc.next();

				//hora inicial
				System.out.println("Ingrese la hora inicial (Ej: 09:00:00.000)");
				String horaInicialReq3B = sc.next();

				//fecha final
				System.out.println("Ingrese la fecha final (Ej : 2017-02-01)");
				String fechaFinalReq3B = sc.next();

				//hora final
				System.out.println("Ingrese la hora final (Ej: 09:00:00.000)");
				String horaFinalReq3B = sc.next();

				try {
					Date fechita8 = sdf.parse(fechaInicialReq3B+'T'+horaInicialReq3B);
					Date fechita8b = sdf.parse(fechaFinalReq3B+'T'+horaFinalReq3B);
					RangoFechaHora rango3B= new RangoFechaHora(fechaInicialReq3B, fechaFinalReq3B);


					ServiciosValorPagado[] resp=Controller.darServiciosZonaValorTotal(rango3B, zona3B);
					for (int i = 0; i < resp.length; i++) {
						System.out.println(resp[i]+"\n");
					}
				} catch (Exception e) {
					// TODO: handle exception
					System.out.println("Fallo el 3B de view :v");
				}

				break;

			case 9: //4B
				//fecha inicial
				System.out.println("Ingrese la fecha inicial (Ej : 2017-02-01)");
				String fechaInicialReq4B = sc.next();

				//hora inicial
				System.out.println("Ingrese la hora inicial (Ej: 09:00:00.000)");
				String horaInicialReq4B = sc.next();

				//fecha final
				System.out.println("Ingrese la fecha final (Ej : 2017-02-01)");
				String fechaFinalReq4B = sc.next();

				//hora final
				System.out.println("Ingrese la hora final (Ej: 09:00:00.000)");
				String horaFinalReq4B = sc.next();

				try {
					Date fechita9 = sdf.parse(fechaInicialReq4B+"T"+horaInicialReq4B);
					Date fechita9b = sdf.parse(fechaFinalReq4B+"T"+horaFinalReq4B);

					RangoFechaHora rango4B= new RangoFechaHora(fechaInicialReq4B, fechaFinalReq4B);

					LinkedList<ZonaServicios> lista4B= Controller.darZonasServicios(rango4B);

					Nodo<ZonaServicios>nodito = lista4B.primero();
					int t = lista4B.size();
					while(t!=0)
					{
						Nodo<FechaServicios> fech = nodito.darItem().getFechasServicios().primero();
						int t2 = nodito.darItem().getFechasServicios().size();
						String zn = nodito.darItem().getIdZona();
						while(t2!=0)
						{
							String fehca = nodito.darItem().getPick()+"";
							fehca = fehca.substring(0,10);
							System.out.println("El numero de servicios que se iniciaron el "+ nodito.darItem().getPick() +"y se iniciaron en la zona "+zn+"        fueron: " + nodito.darItem().getFechasServicios());

							nodito = nodito.next();
							t2--;
						}

						nodito = nodito.next();
						t--;
					}
				}
				catch (Exception e) {
					// TODO: handle exception
					System.out.println("4B :vvvvvvvv");
				}


				break;

			case 10: //2C
				System.out.println("Ingrese el n�mero n de compa�ias");
				int n2C=sc.nextInt();

				//fecha inicial
				System.out.println("Ingrese la fecha inicial (Ej : 2017-02-01)");
				String fechaInicialReq2C = sc.next();

				//hora inicial
				System.out.println("Ingrese la hora inicial (Ej: 09:00:00.000)");
				String horaInicialReq2C = sc.next();

				//fecha final
				System.out.println("Ingrese la fecha final (Ej : 2017-02-01)");
				String fechaFinalReq2C = sc.next();

				//hora final
				System.out.println("Ingrese la hora final (Ej: 09:00:00.000)");
				String horaFinalReq2C = sc.next();

				try {
					Date fechita9 = sdf.parse(fechaInicialReq2C+'T'+horaInicialReq2C);
					Date fechita9b = sdf.parse(fechaFinalReq2C+'T'+horaFinalReq2C);

					RangoFechaHora rango2C= new RangoFechaHora(fechaInicialReq2C, fechaFinalReq2C);

					LinkedList<CompaniaServicios> lista2C= Controller.companiasMasServicios(rango2C, n2C);

					Nodo<Compania> re = lista2C.primero();
					for (int i = 0; i < lista2C.size(); i++) {
						System.out.println(re.darItem().getNombre()+": "+ re.darItem().getNumSer(rango2C).size());
						re = re.next();
					}
				} catch (Exception e) {
					// TODO: handle exception
				}
				break;

			case 11: //3C
				LinkedList<CompaniaTaxi> lista3C=Controller.taxisMasRentables();

				Nodo<Taxi>xdx = lista3C.primero();
				int alv = lista3C.size();
				while(alv!= 0)
				{
					System.out.println("EL taxi mas rentable de la compa�ia "+ xdx.darItem().getCompany()+ " es: "+ xdx.darItem().getTaxiId() );
					alv--;
					xdx = xdx.next();
				}
				break;

			case 12: //4C

				//id taxi
				System.out.println("Ingrese el id del taxi");
				String idTaxi4C=sc.next();

				//fecha 
				System.out.println("Ingrese la fecha inicial (Ej : 2017-02-01)");
				String fechaReq4C = sc.next();

				//hora inicial
				System.out.println("Ingrese la hora inicial (Ej: 09:00:00.000)");
				String horaInicialReq4C = sc.next();

				//hora final
				System.out.println("Ingrese la hora final (Ej: 09:00:00.000)");
				String horaFinalReq4C = sc.next();

				IStack <Servicio> resp4C=Controller.darServicioResumen(idTaxi4C, horaInicialReq4C, horaFinalReq4C, fechaReq4C);
				//TODO
				//Mostrar la informacion de acuerdo al enunciado	
				System.out.println(":'v");

				break;

			case 13: //salir
				fin=true;
				sc.close();
				break;

			}
		}
	}
	/**
	 * Menu 
	 */
	private static void printMenu() //
	{
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Proyecto 1----------------------");
		System.out.println("Cargar data (1C):");
		System.out.println("1. Cargar toda la informacion dada una fuente de datos (small,medium, large).");

		System.out.println("\nParte A:\n");
		System.out.println("2.Obtenga una lista con todos los servicios de taxi ordenados cronologicamente por su fecha/hora inicial, \n"
				+ " que se prestaron en un periodo de tiempo dado por una fecha/hora inicial y una fecha/hora final de consulta. (1A) ");
		System.out.println("3.Dada una compania y un rango de fechas y horas, obtenga  el taxi que mas servicios inicio en dicho rango. (2A)");
		System.out.println("4.Consulte la compania, dinero total obtenido, servicios prestados, distancia recorrida y tiempo total de servicios de un taxi dados su id y un rango de fechas y horas. (3A)");
		System.out.println("5.Dada una fecha y un rango de horas, obtenga una lista de rangos de distancia en donde cada pocision contiene los servicios de taxis cuya distancia recorrida pertence al rango de distancia. (4A)\n");

		System.out.println("Parte B: \n");
		System.out.println("6. Obtenga el numero de companias que tienen al menos un taxi inscrito y el numero total de taxis que trabajan para al menos una compania. \n"
				+ "Luego, genera una lista (en orden alfabetico) de las companias a las que estan inscritos los servicios de taxi. Para cada una, indique su nombre y el numero de taxis que tiene registrados. (1B)");
		System.out.println("7. Dada una compania, una fecha/hora inicial y una fecha/hora final, buscar el taxi de la compania que mayor facturacion gener� en el tiempo dado. (2B)");
		System.out.println("8. Dada una zona de la ciudad, una fecha/hora inicial y una fecha/hora final, dar la siguiente informacion: (3B) \n"
				+ "   -Numero de servicios que iniciaron en la zona dada y terminaron en otra zona, junto con el valor total pagado por los usuarios. \n"
				+ "   -Numero de servicios que iniciaron en otra zona y terminaron en la zona dada, junto con el valor total pagado por los usuarios. \n"
				+ "   -Numero de servicios que iniciaron y terminaron en la zona dada, junto con el valor total pagado por los usuarios.");
		System.out.println("9. Dado un rango de fechas, obtener la lista de todas las zonas, ordenadas por su identificador. Para cada zona, dar la lista de fechas dentro del rango (ordenadas cronol�gicamente) \n "
				+ "y para cada fecha, dar el numero de servicios que se realizaron en dicha fecha. (4B)");


		System.out.println("\nParte C: \n");
		System.out.println("10. Dado un numero n, una fecha/hora inicial y una fecha/hora final, mostrar las n companias que mas servicios iniciaron dentro del rango. La lista debe estar ordenada descendentemente \n por el numero de servicios. Para cada compania, dar el nombre y el numero de servicios (2C)");
		System.out.println("11. Para cada compania, dar el taxi mas rentable. La rentabilidad es dada por la relacion entre el dinero ganado y la distancia recorrida. (3C)");
		System.out.println("12. Dado un taxi, dar el servicio resumen, resultado de haber comprimido su informacion segun el enunciado. (4C) ");
		System.out.println("13. Salir");
		System.out.println("Type the option number for the task, then press enter: (e.g., 1):");

	}

	private static void printMenuCargar()
	{
		System.out.println("-- Que fuente de datos desea cargar?");
		System.out.println("-- 1. Small");
		System.out.println("-- 2. Medium");
		System.out.println("-- 3. Large");
		System.out.println("-- Type the option number for the task, then press enter: (e.g., 1)");
	}

}