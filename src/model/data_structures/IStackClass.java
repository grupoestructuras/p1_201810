package model.data_structures;

public class IStackClass <T extends Comparable<T>> implements IStack {

	Nodo top;
	
	int size;
	
	public IStackClass() {
		top = null;
		size = 0;
	}
	@Override
	public void push(Comparable item) {
		Nodo nod = new Nodo(item);
		if(isEmpty())
			top = nod;
		else {
			nod.cambiarNext(top);
			top = nod;
		}
		size++;
	}

	@Override
	public Comparable pop() {
		if(isEmpty())
			return null;
		Nodo resp = top;
		top = top.next();
		resp.cambiarNext(null);
		size--;
		return resp.darItem();
	}

	@Override
	public boolean isEmpty() {		
		return size == 0?true:false;
	}
	
	public int size()
	{
		return size;
	}

}
