package model.data_structures;

public class LinkedListClass <T extends Comparable> implements LinkedList {

	private Nodo ultimo;

	private Nodo primero;

	private int size;

	public LinkedListClass() {
		ultimo = null;
		primero = null;
		size = 0;
	}

	@Override
	public void add(Comparable item) {
		Nodo naw = new Nodo(item);
		if(primero == null) {
			primero = naw;
			ultimo = naw;
		}					
		else {
			ultimo.cambiarNext(naw);
			ultimo = naw;
		}
		size++;
	}

	@Override
	public void delete(Comparable item) {
		if(primero == null)
			return;
		else if(primero.darItem().equals(item)) {
			Nodo a = primero;
			primero = primero.next();
			a.cambiarNext(null);
			size--;
			if(primero == null)
				ultimo = null;
			return;
		}
		Nodo act = primero;
		while(act.next() != null) {
			if(act.next().darItem().equals(item)) {
				Nodo b = act.next();
				act.cambiarNext(act.next().next());
				b.cambiarNext(null);
				size--;
				return;
			}				
			if(act.next() != null)
				act= act.next();
		}
	}

	@Override
	public Comparable get(Comparable item) {
		if(item == null)
			return null;
		Nodo act = primero;
		while(act != null)
			if (item.equals(act.darItem()))
				return act.darItem();
			else
				act = act.next();
		return null;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public Comparable get(int pos) {	
		int su=0;
		Nodo act = primero;
		while(act != null) {
			if(su==pos)
				return act.darItem();
			act = act.next();
			su++;
		}
		return null;
	}

	@Override
	public Comparable[] listing() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Comparable getCurrent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Comparable next() {


		return null;
	}

	@Override
	public Nodo primero() {		
		return primero;
	}

	@Override
	public Nodo ultimo() {
		return ultimo;
	}

	@Override
	public void set(int pos, Comparable los) {
		Nodo lost = new Nodo(los);
		if(primero == null)
			return;
		else if(pos == 0) {
			lost.cambiarNext(primero.next());
			primero = lost;
		}
		else {
			Nodo act = primero;
			int cont=1;
			while(cont == pos) {	
				act = act.next();
				cont++;
				if(act == null)
					return;
			}
			lost.cambiarNext(act.next().next());
			act.next().cambiarNext(null);
			act.cambiarNext(lost);
		}
	}


}
