package model.data_structures;

import java.util.Iterator;

public class IQueueClass <T extends Comparable<T>> implements IQueue{

	Nodo frente;
	
	Nodo cola;
	
	int size;
	
	public IQueueClass () {
		frente = null;
		cola = null;
		size = 0;
	}
	
	@Override
	public void enqueue(Comparable item) {
		Nodo nod = new Nodo(item);
		if(isEmpty()) {
			frente = nod;
			cola = nod;
		}
		else {
			cola.cambiarNext(nod);
			cola = nod;
		}				
		size++;
		
	}

	@Override
	public Comparable dequeue() {
		if(isEmpty())
			return null;
		Nodo nod = frente;
		frente = frente.next();
		nod.cambiarNext(null);
		if(frente == null)
			cola = null;
		size--;
		return nod.darItem();
	}

	@Override
	public boolean isEmpty() {
		return size == 0? true:false;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public Iterator iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Nodo gethead() {
		// TODO Auto-generated method stub
		return null;
	}

}
