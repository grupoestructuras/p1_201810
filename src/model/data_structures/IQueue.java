package model.data_structures;

import java.util.Iterator;

import model.vo.Servicio;

public interface IQueue <T extends Comparable<T>> 
{
	/**
	 * Agregar un item al tope de la cola
	 */
	public void enqueue(T item);

	/**
	 * Elimina el elemento al tope de la cola
	 */
	public T dequeue();

	/**
	 * Indica si la cola esta vacia
	 */
	public boolean isEmpty();

	/**
	 * Numeros de elementos en la lista
	 */
	public int size();

	public Iterator<Servicio> iterator();

	public Nodo<Servicio> gethead();

	
}