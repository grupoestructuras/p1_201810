package model.vo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import model.data_structures.LinkedList;
import model.data_structures.LinkedListClass;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>
{
	//Atributos
	
	private String taxi_ID;
	
	private String company;
	
	private LinkedList<Servicio> servicios;
	
	private double metros;
	
	private double costo;
	
	public void cargarTaxi(String taxiId, String compania) {
		servicios = new LinkedListClass<Servicio>();
		taxi_ID = taxiId;
		company = compania;
		costo = 0;
		metros = 0;
	}
	
	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId()
	{
		// TODO Auto-generated method stub
		return taxi_ID;
	}

	public void costoyMetros(double pCosto, double pMetros)
	{
		costo += pCosto;
		metros += pMetros;
	}
	
	public double getCosto() {
		return costo;
	}
	
	public double getMetros() {
		return metros;
	}
	/**
	 * @return company
	 */
	public String getCompany()
	{
		// TODO Auto-generated method stub
		return company;
	}
	
	public LinkedList <Servicio> getServices(){
		return servicios;
	}
	
	public void getNumSer(RangoFechaHora rango, LinkedList<Servicio> list) {
		for(int i=0; i < servicios.size(); i++) {
			Servicio serv = servicios.get(i);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sss");
			try
			{
				Date ini = toGregor(serv.start());
				Date fin = toGregor(serv.end());
				if(ini.after(sdf.parse(rango.getFechaInicial()))&& fin.before(sdf.parse(rango.getFechaFinal())))
					list.add(serv);	
			}
			catch(Exception e)
			{
				System.out.println("No se parseo la fecha de getNumSer en taxi");
			}
				
		}
	}
	
	public Date toGregor (String pFecha){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sss", Locale.US);
		Date d = null;
		try {
			d = sdf.parse(pFecha);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return d;
	}
	@Override
	public int compareTo(Taxi o) 
	{
		// TODO Auto-generated method stub
		return 0;
	}	
}