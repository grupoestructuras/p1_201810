package model.vo;

import java.text.SimpleDateFormat;

/**
 * Representation of a Service object
 */
public class Servicio implements Comparable<Servicio>
{	
	private String trip_id, taxi_id;
	
	private int tripSeconds;
	
	private double tripMiles;
	private double tripTotal;
	
	private String trip_start_timesptamp;
	
	private String trip_end_timestamp;
	
	private String zonaIni;
	
	private String zonaDFin;
	

	public void cargarSer(String tripId, String tripSec, String tripMil, String triptrip, String timeS, String timeE, String idZP, String idZD) {
		trip_id = tripId;
		zonaIni = idZP;
		zonaDFin	 = idZD;
		if(!(tripSec == null)) tripSeconds = Integer.parseInt(tripSec);
		if(!(tripMil == null)) tripMiles = Double.parseDouble(tripMil);
		if(!(triptrip == null)) tripTotal = Double.parseDouble(triptrip);
		trip_start_timesptamp = timeS;
		trip_end_timestamp = timeE;
	}
	/**
	 * @return id - Trip_id
	 */
	public String getTripId()
	{
		// TODO Auto-generated method stub
		return trip_id;
	}	
	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return taxi_id;
	}	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		// TODO Auto-generated method stub
		return tripSeconds;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		// TODO Auto-generated method stub
		return tripMiles;
	}
	
	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		// TODO Auto-generated method stub
		return tripTotal;
	}

	public String getZonaIni()
	{
		return zonaIni;
	}
	
	public String getZonaFin()
	{
		return zonaDFin;
	}
	
	public String start() {
		// TODO Auto-generated method stub
		return trip_start_timesptamp;
	}
	
	public String end() {
		// TODO Auto-generated method stub
		return trip_end_timestamp;
	}
	@Override
	public int compareTo(Servicio arg0) {
		// TODO Auto-generated method stub
		return 0;
	}


}
