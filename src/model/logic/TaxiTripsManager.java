package model.logic;

import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Locale;

import com.google.gson.Gson;

import api.ITaxiTripsManager;
import model.data_structures.IQueue;
import model.data_structures.IQueueClass;
import model.data_structures.IStack;
import model.data_structures.IStackClass;
import model.data_structures.LinkedList;
import model.data_structures.LinkedListClass;
import model.data_structures.Nodo;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.Compania;
import model.vo.CompaniaServicios;
import model.vo.CompaniaTaxi;
import model.vo.FechaServicios;
import model.vo.InfoTaxiRango;
import model.vo.Item;
import model.vo.RangoDistancia;
import model.vo.RangoFechaHora;
import model.vo.Servicio;
import model.vo.ServiciosValorPagado;
import model.vo.Taxi;
import model.vo.ZonaServicios;

public class TaxiTripsManager implements ITaxiTripsManager 
{
	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large.json";
	private LinkedList <Compania> companias;

	@Override //1C
	public boolean cargarSistema(String serviceFile) 
	{		
		try (JsonReader reader=new JsonReader(new FileReader(serviceFile));){
			Gson gson = new GsonBuilder().create();
			// Read file in stream mode	 	        
			Item item = new Item();
			reader.beginArray();
			companias = new LinkedListClass<Compania>();
			LinkedListClass<String> taxis = new LinkedListClass<String>();
			LinkedListClass<String> comps = new LinkedListClass<String>();
			LinkedListClass<Taxi> taxies = new LinkedListClass<Taxi>();
			while (reader.hasNext()) {	        	
				item = gson.fromJson(reader, Item.class);    
				if(item.compa�ia() == null)
					item.camCompa�ia( Item.SIN_COM);
				Servicio ser = new Servicio();
				Taxi tax = new Taxi();
				ser.cargarSer(item.tripId(), item.time(), item.miles(), item.tripT(), item.start(), item.end(), item.pickA(), item.dropA());
				if(taxis.get(item.taxiId()) == null) {
					taxis.add(item.taxiId());
					tax.cargarTaxi(item.taxiId(), item.compa�ia());	            	
					tax.getServices().add(ser);
					taxies.add(tax);
				}
				else{
					for(int i=0; i < taxies.size(); i++) {
						tax = (Taxi) taxies.get(i);
						if(item.taxiId().equals(tax.getTaxiId())){
							tax.getServices().add(ser);
							i = taxies.size();
						}
					}
				}
				if(item.tripT() != null && item.miles() != null)
					tax.costoyMetros( Double.parseDouble(item.tripT()), Double.parseDouble(item.miles()));
				Compania com = new Compania();
				if(comps.get(tax.getCompany()) == null) {
					comps.add(tax.getCompany());
					com.cargarCompania(tax.getCompany());
					com.getTaxisInscritos().add(tax);
					companias.add(com);
				}
				else {
					for(int i=0; i < companias.size(); i++) 
						if(tax.getCompany().equals(companias.get(i).getNombre())){
							com = companias.get(i);
							com.getTaxisInscritos().add(tax);
							i = companias.size();
						}
				}
			}	        	        
			reader.close();
		} catch (IOException ex) {
			return false;
		}
		ordenarPorNombre();
		return true;		
	}

	@Override //1A
	public IQueue <Servicio> darServiciosEnPeriodo(RangoFechaHora rango)
	{
		// TODO Auto-generated method stub
		IQueueClass<Servicio> copy = new IQueueClass<Servicio>();
		Date rangoI = toGregor(rango.getFechaInicial());
		Date rangoE = toGregor(rango.getFechaFinal());
		try {
			Servicio serv;
			for(int i=0;i<companias.size()-1;i++)
			{
				Compania companiaI = companias.get(i);
				LinkedList <Taxi> taxisI = companiaI.getTaxisInscritos();
				for (int j=0;j<taxisI.size()-1;j++)
				{
					Taxi taxiI = taxisI.get(j);
					LinkedList <Servicio> services = taxiI.getServices();
					for(int k=0;k<services.size()-1;k++)
					{
						serv= services.get(k);
						String timeI = serv.start();
						String timeE = serv.end();
						Date fechaI = toGregor(timeI);
						Date fechaE = toGregor(timeE);

						if(fechaI.after(rangoI)&&fechaE.before(rangoE))
						{
							copy.enqueue(serv);
						}
					}

				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return copy;

	}


	@Override //2A
	public Taxi darTaxiConMasServiciosEnCompaniaYRango(RangoFechaHora rango, String company)
	{
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		Date rangoI = toGregor(rango.getFechaInicial());
		Date rangoE = toGregor(rango.getFechaFinal());
		int cantidadServices = 0;
		Taxi maxSerTaxi = null;

		try{
			Servicio serv;

			for (int j=0;j<companias.size()-1;j++)
			{
				Compania compj = companias.get(j);
				if(compj.getNombre().equals(company))
				{
					LinkedList <Taxi> taxisI = compj.getTaxisInscritos();
					for (int i=0; i<taxisI.size()-1;i++)
					{
						Taxi taxiI = taxisI.get(i);
						LinkedList <Servicio> services = taxiI.getServices();
						int cantidadser = 0;
						for(int k=0;k<services.size()-1;k++)
						{
							serv= services.get(k);
							String timeI = serv.start();
							String timeE = serv.end();
							Date fechaI = toGregor(timeI);
							Date fechaE = toGregor(timeE);

							if(fechaI.after(rangoI)&&fechaE.before(rangoE))
							{
								cantidadser++;
							}

						}
						if(cantidadser>cantidadServices)
						{
							cantidadServices = cantidadser;
							maxSerTaxi = taxiI;
						}
					}
				}

			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return maxSerTaxi;

	}


	@Override //3A
	public InfoTaxiRango darInformacionTaxiEnRango(String id, RangoFechaHora rango)
	{	
		Taxi tax = buscarTaxi(id);
		InfoTaxiRango resp = new InfoTaxiRango();
		if(tax != null) {		
			resp.setCompany(tax.getCompany());
			resp.setIdTaxi(id);
			resp.setPlataGanada(tax.getCosto());
			resp.setDistanciaTotalRecorrida(tax.getMetros());
			LinkedList<Servicio> inRang = new LinkedListClass<Servicio>();
			resp.setServiciosPrestadosEnRango(inRang);
		}
		return resp;

	}

	@Override //4A
	public LinkedList<RangoDistancia> darListaRangosDistancia(String fecha, String horaInicial, String horaFinal) 
	{
		String fechaInicial = fecha +'T'+horaInicial;
		String fechaFinal = fecha +'T'+ horaFinal;
		Date rangoI = toGregor(fechaInicial);
		Date rangoE = toGregor(fechaFinal);
		int cantidadServices = 0;
		Taxi maxSerTaxi = null;
		LinkedList <RangoDistancia> rango = new LinkedListClass <RangoDistancia>();
		RangoDistancia menorRango = new RangoDistancia();
		RangoDistancia mayorRango = new RangoDistancia();
		menorRango.setLimineInferior(0);
		menorRango.setLimiteSuperior(1);
		mayorRango.setLimineInferior(1);
		mayorRango.setLimiteSuperior(2);
		LinkedList <Servicio> rangos1 = new LinkedListClass <Servicio>();
		LinkedList <Servicio> rangos2 = new LinkedListClass <Servicio>();
		try{
			Servicio serv;

			for (int j=0;j<companias.size()-1;j++)
			{
				Compania compj = companias.get(j);
				{
					LinkedList <Taxi> taxisI = compj.getTaxisInscritos();
					for (int i=0; i<taxisI.size()-1;i++)
					{
						Taxi taxiI = taxisI.get(i);
						LinkedList <Servicio> services = taxiI.getServices();
						int cantidadser = 0;

						for(int k=0;k<services.size()-1;k++)
						{
							serv= services.get(k);
							String timeI = serv.start();
							String timeE = serv.end();
							Date horaI = toGregor(timeI);
							Date horaE = toGregor(timeE);

							if(horaI.after(rangoI)&&horaE.before(rangoE))
							{
								if (serv.getTripMiles()<1 && serv.getTripMiles()>0)
								{
									rangos1.add(serv);
								}
								else if (serv.getTripMiles()<1 && serv.getTripMiles()>0)
								{
									rangos2.add(serv);							}
							}

						}
					}
				}

			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}


		return rango;

	}

	@Override //1B
	public LinkedList<Compania> darCompaniasTaxisInscritos() 
	{		
		return companias;
	}

	@Override //2B
	public Taxi darTaxiMayorFacturacion(RangoFechaHora rango, String nomCompania) 
	{
		Date rangoI = toGregor(rango.getFechaInicial());
		Date rangoE = toGregor(rango.getFechaFinal());
		Compania com = buscarCompania(nomCompania);
		double major = 0;
		Taxi resp = null;
		for (int i = 0; i < com.getTaxisInscritos().size(); i++) {
			Taxi tax = com.getTaxisInscritos().get(i);
			double cont = 0;
			for (int j = 0; j < tax.getServices().size(); j++) {
				Servicio serv = tax.getServices().get(j);
				Date starti = toGregor(serv.start());
				Date endi = toGregor(serv.end());
				if(starti.after(rangoI) && endi.before(rangoE)&& rangoI!=null && rangoE !=null)
					cont += serv.getTripTotal();
			}
			if(cont>major) {
				resp = tax;
				major = cont;}
		}

		return resp;
	}
	@Override //3B
	public ServiciosValorPagado[] darServiciosZonaValorTotal(RangoFechaHora rango, String idZona)
	{
			ServiciosValorPagado[] resp = new ServiciosValorPagado[2];
			@SuppressWarnings("unchecked")
			LinkedList<Servicio> listones = new LinkedListClass<Servicio>();
			LinkedList<Servicio> cero = new LinkedListClass<>();
			LinkedList<Servicio> uno = new LinkedListClass<>();
			LinkedList<Servicio> dos = new LinkedListClass<>();
			double cont1=0;
			double cont2=0;
			double cont3=0;
			for (int i = 0; i < companias.size(); i++) {
				Compania xd = companias.get(i);
				for (int j = 0; j < xd.getTaxisInscritos().size(); j++) {
					Taxi taxix = xd.getTaxisInscritos().get(j);
					taxix.getNumSer(rango, listones);
				}
			}
			for (int i = 0; i < listones.size(); i++) {
				Servicio sad = (Servicio) listones.get(i);
				if(sad.getZonaIni()==idZona && !(sad.getZonaIni()==idZona)) {
					cont1+=sad.getTripTotal();
					cero.add(sad);}
				else if(!(sad.getZonaIni()==idZona) && sad.getZonaIni()==idZona ) {
					cont2 += sad.getTripTotal();
					uno.add(sad);}
				else {
					cont3 += sad.getTripTotal();
					dos.add(sad);
				}
			}
			ServiciosValorPagado zero = new ServiciosValorPagado(cont1, cero);
			ServiciosValorPagado one = new ServiciosValorPagado(cont2, uno);
			ServiciosValorPagado two = new ServiciosValorPagado(cont3, dos);
			resp[0]= zero;
			resp[1]= one;
			resp[2]= two;

			return resp;
		}
	

	@Override //4B
	public LinkedListClass<ZonaServicios> darZonasServicios(RangoFechaHora rango)
	{
		LinkedListClass<ZonaServicios> zonas = new LinkedListClass <ZonaServicios> ();
		Iterator <Servicio> iter = darServiciosEnPeriodo(rango).iterator();
		while(iter.hasNext())
		{
			Servicio actual = iter.next();
			if(actual.getZonaIni()!=null)
			{
				ZonaServicios actualZonA = new ZonaServicios();
				actualZonA.setIdZona(actual.getZonaIni());
				FechaServicios newFech = new FechaServicios();
				newFech.setFecha(actual.start());
				actualZonA.addFecha(newFech);
				if(!existeZona(zonas, actual.getZonaIni()))
				{
					zonas.add(actualZonA);
				}
				
			}
		}
		return zonas;
	}
	
	public boolean existeZona (LinkedList<ZonaServicios> pZonas, String pID)
	{
		boolean resp = false;
	 for (int i=0;i<pZonas.size()-1;i++)
	 {
		 if (pZonas.get(i).getIdZona().equals(pID))
		 {
			resp = true; 
		 }
	 }
	 return resp;
	}

	@Override //2C
	public LinkedList<CompaniaServicios> companiasMasServicios(RangoFechaHora rango, int n)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override //3C
	public LinkedList<CompaniaTaxi> taxisMasRentables()
	{
		LinkedList<CompaniaTaxi> resp = new LinkedListClass<CompaniaTaxi>();
		for(int i=0; i < companias.size(); i++) {
			Compania com = companias.get(i);
			Taxi mayor = com.getTaxisInscritos().get(0);
			for(int j=1; j < com.getTaxisInscritos().size(); j++) {
				Taxi comp = com.getTaxisInscritos().get(j);
				if(mayor.compareTo(comp) < 0)
					mayor = comp;
			}
			CompaniaTaxi net = new CompaniaTaxi(com.getNombre(),mayor);
			resp.add(net);
		}
		return resp;
	}

	@Override //4C
	public IStack <Servicio> darServicioResumen(String taxiId, String horaInicial, String horaFinal, String fecha) 
	{
		LinkedList<Servicio> tax = buscarTaxi(taxiId).getServices();
		IStack<Servicio> resp = new IStackClass<Servicio>();
		String fechaI = fecha+"T"+horaInicial;
		String fechaF = fecha+"T"+horaFinal;
		for(int i=0; i<tax.size(); i++) {			
			if(true)
				resp.push(tax.get(i));
		}
		ordenar(resp);
		return resp;
	}

	private Taxi buscarTaxi(String taxiId) {
		for(int j=0; j < companias.size(); j++) {
			LinkedList<Taxi> com = companias.get(j).getTaxisInscritos();
			for(int i=0; i < com.size(); i++) 
				if(taxiId.equals( ((Taxi)com.get(i)).getTaxiId() )){
					return com.get(i);
				}
		}		
		return null;
	}

	private Compania buscarCompania(String comp) {
		for(int i=0; i < companias.size(); i++) {
			if(comp.equals( companias.get(i).getNombre() )){
				return companias.get(i);
			}
		}		
		return null;
	}

	public void ordenar( IStack<Servicio> list) {
		LinkedList<Servicio> ns = new LinkedListClass<Servicio>();

		int i=0;
		while(!list.isEmpty()) {
			ns.set(i, list.pop());
		}
		LinkedList<Servicio> nss = ns;
		sort(ns, nss,0, ns.size());
		for(i=ns.size()-1; i>=0; i--) {
			list.push(ns.get(i));
		}

	}

	private void ordenarPorNombre( ) {
		LinkedList<Compania> comps = companias;
		sort( companias, comps, 0,companias.size());
	}

	private static void merge(LinkedList a, LinkedList aux, int lo, int mid, int hi){
		int i = lo, j = mid+1;
		for (int k = lo; k <= hi; k++) {
			if (i > mid) aux.set(k, a.get(j++));
			else if (j > hi) aux.set(k, a.get(i++));
			else if ((a.get(j).compareTo(a.get(i)) < 0)) aux.set(k, a.get(j++));
			else aux.set(k,  a.get(i++));
		}
	}
	private static void sort(LinkedList a, LinkedList aux, int lo, int hi) {
		if (hi <= lo) return;
		int mid = lo + (hi - lo) / 2;
		sort (a, aux, lo, mid);
		sort (a, aux, mid+1, hi);
		merge(a, aux, lo, mid, hi);
	}
	public Date toGregor (String pFecha){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sss", Locale.US);
		Date d = null;
		try {
			d = sdf.parse(pFecha);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return d;
	}

}


